
#include <ESP8266WiFi.h>
 
const char* ssid     = "Sachin";
const char* password = "sachin123";

//const char* ssid     = "Whats Up DomBabes";
//const char* password = "3lcy5tillrocks!";


const char* host = "www.adafruit.com";

char server[] = "www.smartify.info";

WiFiClient client;

int sensorPin = A0;    // Input pin for analog sensor
float sensorValue = 0;   // Variable to store sensor value
const int resetButtonPin = 4;     // the number of the pushbutton pin for reset
float resetWeight = 0;      // Weight recorded when the reset button is pressed
int isWeightLow = 0;
float previous_value = 0.0;

void setup() {
  Serial.begin(115200);
  delay(100);
 
  // We start by connecting to a WiFi network
  connectToWiFi();
  
  // initialize the pushbutton pin as an input:
  pinMode(resetButtonPin, INPUT);
  
}

void post_smartify(float percentage_left, int sensor_id) {
  Serial.println("\nInside post function");
  while(1){
    if (client.connect(server, 80)) {
      Serial.println("Connected to smartify");
      //String PostData="item";
      String PostData="{\"value\":" + String(percentage_left) + ", \"sensor_id\":" + String(sensor_id) +"}";
      client.println("POST /items.json HTTP/1.1");
      client.println("Host:  www.api.smartify.info");
      client.println("User-Agent: Arduino/1.0");
      client.println("Connection: close");
      //client.println("Content-Type: application/x-www-form-urlencoded;");
      client.println("Content-Type: application/json;charset=utf-8");
      client.print("Content-Length: ");
      client.println(PostData.length());
      client.println();
      client.println(PostData);
      break;
    } else {
      Serial.println("Cannot connect to smartify");
      client.stop();
    }
  }
}
 


void loop() {

  // To check if the reset button is pressed
  if(digitalRead(resetButtonPin)==HIGH){
    Serial.print("\nAbout to call the function");
    //delay(5000);
    isWeightLow = 0;
    readResetValue();
  } else {
    delay(2000);
    sensorValue = analogRead(sensorPin);

    
    
    
    if(resetWeight != 0){
      
      if(sensorValue <= 2){
        int permanent_zero = 1;
        int zero_weight_counter = 0;
        
        //while(zero_weight_counter <= 900){
        while(zero_weight_counter <= 30){
          if (analogRead(sensorPin) > 2){
            permanent_zero = 0;
            break;
          }
          delay(1000);
          zero_weight_counter++;
        }

        if (permanent_zero == 1) {
          Serial.print("\nHey your milk is over!");
          resetWeight = 0;
          //Make a post call
          post_smartify(0.0, 1);
        } else {
          Serial.print("\nNot over yet");
        }
        
      } else if (sensorValue != previous_value) { //if ((sensorValue < previous_value-2) || (sensorValue > previous_value+2)){ 
        
        
          previous_value = sensorValue;
          
          Serial.print("\nCurrent weight on sensor is: ");
          Serial.print(sensorValue);
          Serial.print("\nReset weight on sensor is: ");
          Serial.print(resetWeight);
          Serial.print("\nDivision value is: ");
          Serial.print(sensorValue/resetWeight);
          isWeightLow = 1;
          //Make a post call
          post_smartify(sensorValue/resetWeight, 1);
      }
    }
  }
}

void readResetValue(){
  int maxWeight = analogRead(sensorPin);
  Serial.print("\n***************Entered resetValue function");
  int counter=0;
  while(counter<10){
    if(maxWeight > analogRead(sensorPin)){
      maxWeight = analogRead(sensorPin);
    }
    counter++;
    delay(100);
  }
  if (maxWeight > 1){
    resetWeight = maxWeight;
    previous_value = resetWeight;
    post_smartify(1.0, 1);
  }
 
  Serial.print("\n***************reset weight: ");
  Serial.print(resetWeight);
  
}

void connectToWiFi(){
  Serial.print("Connecting to ");
  Serial.println(ssid);
  
  WiFi.begin(ssid, password);
  
  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }
 
  Serial.println("");
  Serial.println("WiFi connected");  
  Serial.println("IP address: ");
  Serial.println(WiFi.localIP());

  //post_smartify(9.32, 1);
}

